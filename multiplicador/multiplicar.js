const fs = require('fs');
const colores = require('colors');

let listarTabla = (base, limite = 10) => {
    console.log('============='.purple);
    console.log(`Tabla del ${base}`.yellow);
    console.log('============='.purple);
    for (let i = 1; i <= limite; i++) {
        console.log(`${base} * ${i} =`.red, `${base * i}`.green);
    }
}

let crearArchivo = (base, limite = 10) => {
    return new Promise((resolve, reject) => {

        if (!Number(base)) {
            reject(`${base} no es un número`.red);
            return;
        }

        let tabla = '';

        for (let i = 1; i <= limite; i++) {
            tabla += `${base} * ${i} = ${base * i}\n`;
        }


        fs.writeFile(`tablas/tabla-${base}.txt`, tabla, (err) => {
            if (err)
                reject(err)
            else
                resolve(`tabla-${base}.txt`)
        });

    })
};
module.exports = {
    crearArchivo,
    listarTabla
}