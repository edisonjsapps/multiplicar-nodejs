const opciones = {

    base: {
        demand: true,
        alias: 'b'
    },
    limite: {
        alias: 'l',
        default: 10
    }
}

const argv = require('yargs')
    .command('listar', 'Imprime en consola la tabla a multiplicar', opciones)
    .command('crear', 'Crea la tabla de multiplicar', opciones)
    .help()
    .argv;

module.exports = {
    argv
}